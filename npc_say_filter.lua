local NpcSayFilter = {}
local _G = _G
_G.NpcSayFilter = NpcSayFilter

local string = string
function NpcSayFilter.ChatFrame_OnEvent(this, event)
    if event == "CHAT_MSG_SAY" then
        if string.sub(arg2,0,2) ~= "|H" then
            return
        end
    end
    NpcSayFilter.original_ChatFrame_OnEvent(this, event)
end

function NpcSayFilter:Initialize()
    if _G.Sol then
        _G.Sol.hooks.Hook("NpcSayFilter", "ChatFrame_OnEvent", NpcSayFilter.ChatFrame_OnEvent)
        NpcSayFilter.original_ChatFrame_OnEvent = _G.Sol.hooks.GetOriginalFn("NpcSayFilter", "ChatFrame_OnEvent")
    else
        NpcSayFilter.original_ChatFrame_OnEvent = _G.ChatFrame_OnEvent
        _G.ChatFrame_OnEvent = NpcSayFilter.ChatFrame_OnEvent
    end
    print("Initialized NPC Say Filter")
end

function NpcSayFilter:VARIABLES_LOADED(_arg1, _arg2, _arg3, _arg4, _arg5, _arg6, _arg7, _arg8)
    self:Initialize()
end

function NpcSayFilter:OnEvent(event, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8)
    self[event](self, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8)
end

NpcSayFilter.Frame = CreateUIComponent("Frame", "NpcSayFilter_Frame", "UIParent", nil)
local frame = NpcSayFilter.Frame
_G[frame:GetName()] = nil
frame:Hide()
frame:SetScripts("OnEvent", "NpcSayFilter:OnEvent(event, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8)")
frame:RegisterEvent("VARIABLES_LOADED")
